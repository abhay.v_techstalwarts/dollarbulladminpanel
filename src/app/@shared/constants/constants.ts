export class CommonConstants {
    static table = {
        pagingType: 'full_numbers',
        pageLength: 10,
        retrieve: true,
        processing: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5', title: '', text: 'Export to Excel', className: 'btn btn-outline-secondary', exportOptions: {

                    columns: "thead th:not(.noExport)"
                }
            },
        ]
    };
}