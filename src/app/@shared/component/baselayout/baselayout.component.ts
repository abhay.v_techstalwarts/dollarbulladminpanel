import { Component, OnInit ,Input} from '@angular/core';
@Component({
  selector: 'app-baselayout',
  templateUrl: './baselayout.component.html',
})
export class BaselayoutComponent implements OnInit {
  @Input()
  title: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  logout(){
    window.localStorage.clear();
    location.reload();
  }

}
