import { AuthGuard } from './@core/guard/auth.guard';
import { CustomersModule } from './customers/customers.module';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'resepassword',
    component: ResetpasswordComponent,
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule), canActivate: [AuthGuard],
  },
  {
    path: 'partner',
    loadChildren: () => import('./partner/partner.module').then(m => m.PartnerModule), canActivate: [AuthGuard]
  },
  {
    path: 'customer',
    loadChildren: () => import('./customers/customers.module').then((m) => m.CustomersModule), canActivate: [AuthGuard]
  },
  {
    path: 'stock',
    loadChildren: () => import('./stock/stock.module').then((m) => m.StockModule), canActivate: [AuthGuard]
  },
  {
    path: 'margin-customers',
    loadChildren: () => import('./margin-customers/margin-customers.module').then((m) => m.MarginCustomersModule), canActivate: [AuthGuard]
  },
  {
    path: 'stage-count',
    loadChildren: () => import('./stage-count/stage-count.module').then((m) => m.StageCountModule), canActivate: [AuthGuard]
  },
  { path: 'faq', loadChildren: () => import('./faq/faq.module').then((m) => m.FaqModule), canActivate: [AuthGuard] },
  { path: 'videos', loadChildren: () => import('./videos/videos.module').then((m) => m.VideosModule), canActivate: [AuthGuard] },
  //{ path: 'stage-customers', loadChildren: () => import('./stage-customers/stage-customers.module').then(m => m.StageCustomersModule) },
  { path: 'stage-count/stage-customers/:id', loadChildren: () => import('./stage-customers/stage-customers.module').then(m => m.StageCustomersModule), canActivate: [AuthGuard] },
  { path: 'auto-notification', loadChildren: () => import('./auto-notification/auto-notification.module').then(m => m.AutoNotificationModule),canActivate: [AuthGuard] },
  { path: 'custom-notification', loadChildren: () => import('./custom-notification/custom-notification.module').then((m) => m.CustomNotificationModule), canActivate: [AuthGuard]},
  { path: 'beta-users', loadChildren: () => import('./beta-users/beta-users.module').then(m => m.BetaUsersModule), canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
