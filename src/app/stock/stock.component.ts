import { Component, OnInit } from '@angular/core';
import { StockService } from '../@core/service/stock/stock.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { CommonConstants } from '../@shared/constants/constants';


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html'
})
export class StockComponent implements OnInit {
  stocksList: any;
  loader: boolean = true;
  dtOptions = CommonConstants.table;
  dtTrigger: Subject<any> = new Subject<any>();
  search: any;
  delete_modalReference: any;
  activate_modalReference: any;
  stockObj = {
    search: "",
    id: ""
  }
  constructor(
    private stockService: StockService,
    private jwtService: JwtserviceService,
    private toastr: ToastrService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'stock list';
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 20,
    //   processing: true,
    //   dom: 'Bfrtip',
    //   buttons: [
    //     { extend: 'excel', className: 'btn btn-outline-warning' },
    //   ]

    // };
    this.search = "";
    this.stockObj.search = "";
    this.getStocks(this.stockObj);
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';
  }

  getStocks(stock: any) {
    try {

      let offset = 0;
      let data = {
        offset: offset,
        limit: 33000,
        search: stock.search
      }
      this.stockService.stockList(data).subscribe(
        (success: any) => {
          this.loader = false;
          //this.toastr.success(success.message, "Dollarbull", { timeOut: 30000, disableTimeOut: false });
          if (success.status == 1) {
            this.stocksList = success.result.companies;
            this.dtTrigger.next();
          }
        },
        error => {
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

        }

      )
    }
    catch (err: any) {
      console.log(err);
      //  this.toastr.error(err, "", { timeOut: 3000, disableTimeOut: false, });
    }

  }

  deactivateStock(company_id?: any, stock?: any) {
    try {
      console.log(this.stockObj.id, "deactivate")
      let data = {
        company_id: this.stockObj.id.toString(),
        is_active: 0
      }

      this.stockService.deactivateStock(data).subscribe(
        (success: any) => {
          console.log(success);
          if (success.status == 1) {
            this.toastr.success(success.message, "Dollarbull", { timeOut: 3000, disableTimeOut: false, });
            console.log(stock, "deactivate")
            //this.search("");
            this.delete_modalReference.close();
            setTimeout(() => {
            location.reload();
            }, 1000);


          }
          else {
            // this.toastr.error(success.error.message, "", { timeOut: 3000, disableTimeOut: false, });
            this.delete_modalReference.close();
          }
        },
        error => {
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  activateStock(company_id?: any, stock?: any) {
    debugger;
    try {
      let data = {
        company_id: this.stockObj.id.toString(),
        is_active: 1
      }

      this.stockService.deactivateStock(data).subscribe(
        (success: any) => {
          if (success.status == 1) {
            this.toastr.success(success.message, "Dollarbull", { timeOut: 3000, disableTimeOut: false, });
            console.log(stock, "deactivate")
            //this.search("");
            this.activate_modalReference.close();
            setTimeout(() => {
              location.reload();
              }, 1000);
          }
          else {
            // this.toastr.error(success.error.message, "", { timeOut: 3000, disableTimeOut: false, });
            this.activate_modalReference.close();
          }
        },
        error => {
          console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  clear() {
    this.stockObj.search = "";

    this.getStocks("");
  }

  closedeletemodal(content: any) {
    this.delete_modalReference.close();
  }

  showdeleteConfirmPopup(id: any, contentdelete: any) {
    this.stockObj.id = id;
    this.delete_modalReference = this.modalService.open(contentdelete, {
      backdrop: 'static',
      keyboard: false
    });
    this.delete_modalReference.result.then((result2: any) => {
      console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closeactivatemodal(content: any) {
    this.activate_modalReference.close();
  }

  showactivateConfirmPopup(id: any, contentactivate: any) {
    this.stockObj.id = id;
    this.activate_modalReference = this.modalService.open(contentactivate, {
      backdrop: 'static',
      keyboard: false
    });
    this.activate_modalReference.result.then((result2: any) => {
      console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
