import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { MarginCustomersRoutingModule } from './margin-customers-routing.module';
import { MarginCustomersComponent } from './margin-customers.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [
    MarginCustomersComponent
  ],
  imports: [
    CommonModule,
    MarginCustomersRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  ]
})
export class MarginCustomersModule { }
