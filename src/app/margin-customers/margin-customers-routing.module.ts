import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarginCustomersComponent } from './margin-customers.component';

const routes: Routes = [{ path: '', component: MarginCustomersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarginCustomersRoutingModule { }
