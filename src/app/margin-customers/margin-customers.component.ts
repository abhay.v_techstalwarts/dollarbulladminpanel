import { Component, OnInit } from '@angular/core';
import { MarginCustomersService } from '../@core/service/margin-customers/margin-customers.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { Subject } from 'rxjs';
import { CommonConstants } from '../@shared/constants/constants';

@Component({
  selector: 'app-margin-customers',
  templateUrl: './margin-customers.component.html'
})
export class MarginCustomersComponent implements OnInit {
  marginCustomerList: any;
  search: any;
  loader=true;
  dtOptions = CommonConstants.table;
  dtTrigger: Subject<any> = new Subject<any>();
  margincustomerobj = {
    "search": "",
  }
  constructor(
    private marginCustomerService: MarginCustomersService,
    private jwtService: JwtserviceService,
  ) { }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'margin list';
    this.search = "";
    this.getMarginCustomerList(this.search);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';
  }

  getMarginCustomerList(search: any) {
    try {
      let offset = 0;
      let data = {
        offset: offset,
        limit: 1000,
        search: search.search
      }
      this.marginCustomerService.marginCustomersList(data).subscribe(
        (success: any) => {
          this.loader=true;
          // console.log(success);
          if (success.status == 1) {
            this.loader=false;
            this.marginCustomerList = success.result.customer;
            for (let i = 0; i < this.marginCustomerList.length; i++) {
              this.marginCustomerList[i].index = offset + (i + 1);
            }
            this.dtTrigger.next();
          }
        },
        error => {
          // console.log("message");
          console.log(error);
        }

      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  clear() {
    this.margincustomerobj.search = "";

    this.getMarginCustomerList("");
  }

}
