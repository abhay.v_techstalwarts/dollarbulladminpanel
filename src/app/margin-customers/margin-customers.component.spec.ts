import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarginCustomersComponent } from './margin-customers.component';

describe('MarginCustomersComponent', () => {
  let component: MarginCustomersComponent;
  let fixture: ComponentFixture<MarginCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarginCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarginCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
