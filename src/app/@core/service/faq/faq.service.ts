import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  public faqList(data: any) {
    const headers = this.getHeaders();
    // console.log(">>>> data", data)
    return this.http.get(this.apiUrl + '/faq?search=' + data.search + '&page=' + data.offset + '&perPage=' + data.limit, { headers });
  }

  public deleteFAQ(data: any) {
    const headers = this.getHeaders();
    return this.http.delete(this.apiUrl + '/faq?id=' + data, { headers });
  }

  public addFAQ(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/faq', data, { headers });
  }

  public updateFAQ(data: any) {
    const headers = this.getHeaders();
    return this.http.put(this.apiUrl + '/faq', data, { headers });
  }

  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
