import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  getCustomerList(rm_master_id: any): Observable<any> {
    let queryParam = "";
    if (rm_master_id) {
      queryParam = `?rm_master_id=${rm_master_id}`;
    }
    const headers = this.getHeaders();
    return this.http.get(`${this.apiUrl}/customers${queryParam}`, { headers })
  }

  getCustomerPendingOrderList(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/pendingorders', data, { headers })
  }

  getPortfolio(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/getCustomerInvestmentsPieChart', data, { headers })
  }

  getPerformanceChart(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/performance_chart', data, { headers })
  }

  getPortfolioOverview(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/portfolio_overview', data, { headers })
  }

  getInvestedStocks(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/my_investment', data, { headers })
  }

  getCustomerAccount(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/getCustomerAccounts', data, { headers })
  }

  delinkCustomerTradeStation(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/delink-tradestation', data, { headers })
  }

  deactivateAccount(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/deactivateaccount', data, { headers })
  }


  getRM(): Observable<any> {
    const headers = this.getHeaders();
    return this.http.get(this.apiUrl + '/rmlist', { headers })
  }

  updateRM(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/update-rm', data, { headers })
  }


  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
