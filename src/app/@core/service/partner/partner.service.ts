import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
import { CommonServiceService } from '../common/common-service.service';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {


  apiUrl: string = environment.apiUrl;
  httpHeader: any;
  constructor(private http: HttpClient,
    private jwtService: JwtserviceService,
    private _cs: CommonServiceService) {

    this.httpHeader = this._cs.getHeaders();

  }

  // Partner
  getPartnerList(): Observable<any> {
    const headers = this.httpHeader;
    return this.http.get(this.apiUrl + '/partner?page=0&perPage=1000', { headers })
  }

  addPartner(data: any): Observable<any> {
    const headers = this.httpHeader;
    return this.http.post(`${this.apiUrl}/partner`, data, { headers });
  }

  updatePartner(data: any): Observable<any> {
    const headers = this.httpHeader;
    return this.http.put(`${this.apiUrl}/partner`, data, { headers });
  }

  deletePartner(id: any, is_active: any): Observable<any> {
    const headers = this.httpHeader;
    return this.http.delete(`${this.apiUrl}/partner?id=${id}&is_active=${is_active}`, { headers });
  }


  // Partner: RM 
  getRmPartnerList(id: number): Observable<any> {
    const headers = this.httpHeader;
    // let data:any = {partner_id : id};
    return this.http.get(`${this.apiUrl}/partner/rm-list?partner_id=${id}`, { headers });
  }






}
