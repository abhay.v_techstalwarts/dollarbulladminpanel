import { TestBed } from '@angular/core/testing';

import { BetaUsersService } from './beta-users.service';

describe('BetaUsersService', () => {
  let service: BetaUsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BetaUsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
