import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
import { CommonServiceService } from '../common/common-service.service';

@Injectable({
  providedIn: 'root'
})
export class BetaUsersService {
  apiUrl: string = environment.apiUrl;
  httpHeader: any;
  constructor(private http: HttpClient, private jwtService: JwtserviceService, private _cs: CommonServiceService) {
    this.httpHeader = this._cs.getHeaders();
  }

  public betaUserList() {
    const headers = this.httpHeader;
    //  console.log(">>>> data", data)
    return this.http.get(this.apiUrl + '/beta-user-list', { headers });
  }

}
