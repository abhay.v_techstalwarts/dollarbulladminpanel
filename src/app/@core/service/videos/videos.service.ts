import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }


  public videoList(data: any) {
    const headers = this.getHeaders();

    return this.http.get(this.apiUrl + '/video-url?search=' + data.search + '&page=' + data.offset + '&perPage=' + data.limit, { headers });
  }

  public deleteVideo(data: any) {

    const headers = this.getHeaders();

    return this.http.delete(this.apiUrl + '/video-url?id=' + data, { headers });
  }

  public addVideo(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/video-url', data, { headers });
  }

  public updateVideo(data: any) {
    const headers = this.getHeaders();
    return this.http.put(this.apiUrl + '/video-url', data, { headers });
  }

  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }

}
