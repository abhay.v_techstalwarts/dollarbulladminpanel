// Modules
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpResponse } from '@angular/common/http';

// Services
import { JwtserviceService } from '../jwt-token/jwtservice.service';

// Alert notification
import {ToastrService} from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor(private jwtService: JwtserviceService,
              public _alertMsg: ToastrService) 
              { }

  // Get HTTP Headers
  getHeaders(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
       'Access-Control-Allow-Origin' : '*',
       Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }

  // Alert Message
  alertMsg(type:String , apiMsg:String = '', msg:String = ''){

    let msgText: String = '';
    msgText = (apiMsg && apiMsg.constructor == String && apiMsg.length > 0) ?
              apiMsg :  msg;
    
    if(!msgText) return;

    switch(type){
      case 'error':
      this._alertMsg.error(`${msgText}`);
        break;
      case 'success':
      this._alertMsg.success(`${msgText}`);
        break;
      case 'info':
      this._alertMsg.info(`${msgText}`);
        break;
      case 'warning':
      this._alertMsg.warning(`${msgText}`);
        break;

    }

}


  
}
