import { TestBed } from '@angular/core/testing';

import { StageCustomersService } from './stage-customers.service';

describe('StageCustomersService', () => {
  let service: StageCustomersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StageCustomersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
