import { TestBed } from '@angular/core/testing';

import { AutoNotificationService } from './auto-notification.service';

describe('AutoNotificationService', () => {
  let service: AutoNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
