import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';

@Injectable({
  providedIn: 'root'
})
export class AutoNotificationService {

  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  getNotificationList(): Observable<any> {
    let queryParam = "";
    // if (rm_master_id) {
    //queryParam = `?perPage=${perpage}&page=${page}`;
    // }
    const headers = this.getHeaders();
    return this.http.get(`${this.apiUrl}/notification`, { headers })
  }


  updateNotification(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.put(this.apiUrl + '/notification', data, { headers })
  }



  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }

}
