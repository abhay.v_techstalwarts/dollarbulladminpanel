import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// import { environment } from './../environments/environment';
// import { JwtService } from './jwt.service';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient , private jwtService: JwtserviceService,) { }
  Login(data: any): Observable<any> {
    return this.http.post(this.apiUrl+'/login',data)
  }

  
  public getHeaders(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
       'Access-Control-Allow-Origin' : '*',
       Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
