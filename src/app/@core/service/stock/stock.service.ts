import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// import { environment } from './../environments/environment';
// import { JwtService } from './jwt.service';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
@Injectable({
  providedIn: 'root'
})
export class StockService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  public stockList(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/tickers', data);
  }

  public deactivateStock(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/deactivate', data);
  }

  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
