import { TestBed } from '@angular/core/testing';

import { MarginCustomersService } from './margin-customers.service';

describe('MarginCustomersService', () => {
  let service: MarginCustomersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarginCustomersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
