import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';
@Injectable({
  providedIn: 'root'
})
export class MarginCustomersService {

  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  public marginCustomersList(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/margin-customers', data, { headers });
  }

  public marginCustomersExcel(data: any) {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl + '/export-margin-customers', data, { headers });
  }
  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
