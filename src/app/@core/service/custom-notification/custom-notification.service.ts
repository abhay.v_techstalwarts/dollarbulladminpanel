import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtserviceService } from '../jwt-token/jwtservice.service';

@Injectable({
  providedIn: 'root'
})
export class CustomNotificationService {

  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient, private jwtService: JwtserviceService,) { }

  getSubStage() {
    const headers = this.getHeaders();
    return this.http.get(`${this.apiUrl}/notification`, { headers })
  }

  pushNotification(data: any) {
    const headers = this.getHeaders();
    return this.http.post(`${this.apiUrl}/send-notification`, data, { headers })
  }

  public getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
