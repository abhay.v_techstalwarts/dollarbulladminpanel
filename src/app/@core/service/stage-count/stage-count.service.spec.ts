import { TestBed } from '@angular/core/testing';

import { StageCountService } from './stage-count.service';

describe('StageCountService', () => {
  let service: StageCountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StageCountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
