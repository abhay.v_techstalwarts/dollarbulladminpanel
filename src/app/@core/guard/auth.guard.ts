import { JwtserviceService } from './../service/jwt-token/jwtservice.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private JwtService: JwtserviceService , private router: Router){ }
  canActivate(): boolean {
    if(this.JwtService.getToken()){
     return true;
    }
 
    else{
      this.router.navigate(['/login'])
      return false;
    }
  }
  
}
