import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { CommonService } from '../common.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  //@ts-ignore
  password:number;
  hide: boolean = true;
  //@ts-ignore
  confirmPassword:number;
  //@ts-ignore
  errorConfirmPassword:boolean;
  //@ts-ignore
  errorPassword:boolean;
  resetButtonDisabled:boolean=true;
  resetPasswordForm:any;
  reset_code:any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute ,private fb: FormBuilder, private toastr : ToastrService, private commn:  CommonService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.reset_code = params.reset_code;
    });

    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required ,Validators.min(6)]],
      confirmpassword: ['', [Validators.required ,Validators.min(6)]],
    });
  }

  myFunction() {
    this.hide = !this.hide;
  }

  passwordPress(event:any) {
    this.password = event.target.value;
  }
  confirmPasswordPress(event:any){
    this.confirmPassword = event.target.value;
    if( this.confirmPassword != this.password){
      this.resetButtonDisabled = true;
      this.errorConfirmPassword = true;
    }
    else{
      this.resetButtonDisabled = false;
      this.errorConfirmPassword = false;
    }
  }

  submitResetPassword(){  
    //  let email_id = localStorage.getItem('email_id');
      let data = {
        reset_code : this.reset_code,
        password : this.resetPasswordForm.get('confirmpassword').value,
      }
      this.commn.resetPassword(data).subscribe((res:any) => {
        if(res.status == '1'){
        this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
        this.logout();
        }
      },
      (err) => {
        this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
      })
    }
    logout(){
      window.localStorage.clear();
      this.router.navigate(['/login']);
    }
}
