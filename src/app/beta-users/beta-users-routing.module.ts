import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetaUsersComponent } from './beta-users.component';

const routes: Routes = [{ path: '', component: BetaUsersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BetaUsersRoutingModule { }
