import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { BetaUsersRoutingModule } from './beta-users-routing.module';
import { BetaUsersComponent } from './beta-users.component';
import { SharedModule } from '../@shared/shared.module';



@NgModule({
  declarations: [
    BetaUsersComponent
  ],
  imports: [
    DataTablesModule,
    SharedModule,
    CommonModule,
    BetaUsersRoutingModule
  ]
})
export class BetaUsersModule { }
