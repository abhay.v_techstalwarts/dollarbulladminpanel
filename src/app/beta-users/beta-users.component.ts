import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BetaUsersService } from '../@core/service/beta-users/beta-users.service';
import { CommonConstants } from 'src/app/@shared/constants/constants';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-beta-users',
  templateUrl: './beta-users.component.html'
})
export class BetaUsersComponent implements OnInit {
  betaUserList: any = [];
  offset: any;
  loader=true;
  dtOptions = CommonConstants.table;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private betaUserService: BetaUsersService,
  ) { }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'beta users';

    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    //   processing: true,
    //   dom: 'Bfrtip',

    //   buttons: [
    //     { extend: 'excel', className: 'btn btn-outline-warning' },
    //   ]

    // };
    this.getBetaUserList();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';
  }

  getBetaUserList() {
    this.loader =true;
  // console.log(this.dtOptions);

    this.offset = 0;
    this.betaUserService.betaUserList().subscribe(
      (success: any) => {
        // console.log(success);
        if (success.status == 1) {
         this.loader =false;

          this.betaUserList = success.result;
          this.dtTrigger.next();
          for (let i = 0; i < this.betaUserList.length; i++) {
            this.betaUserList[i].index = this.offset + (i + 1);
          }
        }
      },
      error => {
        // console.log("message");
        console.log(error);
        //     this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

}
