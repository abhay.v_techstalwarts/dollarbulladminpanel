import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { PartnerListComponent } from './partner-list/partner-list.component';
import { PartnerRmListComponent } from './partner-rm-list/partner-rm-list.component';
import { CustomersListComponent } from 'src/app/customers/customers-list/customers-list.component';
import { CustomersPendingOrderListComponent } from 'src/app/customers/customers-pending-order-list/customers-pending-order-list.component';
import { CustomersPortfolioComponent } from 'src/app/customers/customers-portfolio/customers-portfolio.component';

const routes: Routes = [
  {path:"", component: PartnerListComponent},
  {path:"rm/:partner_id", component: PartnerRmListComponent},
  {path:"rm/:partner_id/customer/:rm_master_id", component: CustomersListComponent},
  {path:"rm/:partner_id/customer/:rm_master_id/pendingorder/:customer_id", component: CustomersPendingOrderListComponent},
  {path:"rm/:partner_id/customer/:rm_master_id/portfolio/:customer_id", component: CustomersPortfolioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerRoutingModule { }
