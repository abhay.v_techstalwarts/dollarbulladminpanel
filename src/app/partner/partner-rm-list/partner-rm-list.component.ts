// Lib
import { Subject } from 'rxjs';

// Modules
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// Services
import { PartnerService } from 'src/app/@core/service/partner/partner.service';

// Commone
import { CommonConstants } from 'src/app/@shared/constants/constants';
import { CommonServiceService } from 'src/app/@core/service/common/common-service.service';


@Component({
  selector: 'app-partner-rm-list',
  templateUrl: './partner-rm-list.component.html',
  styleUrls: ['./partner-rm-list.component.scss']
})
export class PartnerRmListComponent implements OnInit {
  paramPartner_id: number;
  loader=true;
  partnerList: any;

  dtOptions = CommonConstants.table;

  dtTrigger: Subject<any> = new Subject<any>();

  breadcrumb: any[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private commonService: CommonServiceService,
    private PartnerService: PartnerService) {

    this.paramPartner_id = this.route.snapshot.params.partner_id;
    this.breadcrumb = [{ name: "Partner", link: `/partner` },
    { name: "RM List" }];
  }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'rm list';
    this.getPartnerList();
  }


  getPartnerList() {
    if (!this.paramPartner_id) {
      this.commonService.alertMsg("error", "mandatory parameter is missing");
    }
    this.loader=true;
    this.PartnerService.getRmPartnerList(this.paramPartner_id).subscribe(
      success => {

        if (success.status == 1) {
          this.loader=false;
          this.partnerList = success.result.rm;
          this.dtTrigger.next();
        } else {
          this.commonService.alertMsg("error", success.message, "No data found");
        }

      },
      fail => {
        console.log(fail);
        this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
      }

    )
  }


}
