// Lib
import { Subject } from 'rxjs';

// Modules
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Services
import { PartnerService } from 'src/app/@core/service/partner/partner.service';

// Commone
import { CommonConstants } from 'src/app/@shared/constants/constants';
import { CommonServiceService } from 'src/app/@core/service/common/common-service.service';


@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss']
})
export class PartnerListComponent implements OnInit {
  selectedIndex: any;

  dtOptions = CommonConstants.table;

  dtTrigger: Subject<any> = new Subject<any>();

  partnerList: any;

  Form: any;
  partner_modal: any;
  loader:boolean=true;
  activate_modal: any;
  delete_modal: any;
  id2Delete: any = null;


  constructor(private PartnerService: PartnerService,
    private router: Router,
    private commonService: CommonServiceService,
    private modalService: NgbModal,
    private editorService: EditorModule,
    private _fb: FormBuilder) { }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'partner list';
    this.getPartnerList();
    this.initForm();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';
  }

  initForm() {
    let formGroup = {
      id: [null],
      partner_name: ['', [Validators.required]],
      partner_contact_no: ['', [Validators.required]],
      partner_address: ['', [Validators.required]],
      partner_email_id: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
    }
    this.Form = this._fb.group(formGroup);
  }

  getPartnerList() {
    this.loader=true;
    this.PartnerService.getPartnerList().subscribe(
      success => {
        if (success.status == 1) {
          this.loader=false;
          this.partnerList = success.result.partner;
          this.dtTrigger.next();
          // console.log(this.partnerList);
        } else {
          this.commonService.alertMsg("error", success.message, "No data found");
        }

      },
      fail => {
        console.log(fail);
        this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
      }

    )
  }

  setIndex(index: number , id:any) {
    this.selectedIndex = index;
    this.router.navigate([`/partner/rm/${id}`], { replaceUrl: true });
 }

  partnerFormModal(content: any, data = null) {
    this.partner_modal = this.modalService.open(content, {
      backdrop: 'static',
      keyboard: false
    });
    this.setPartnerForm(data);
  }

  setPartnerForm(data: any = null) {
    if (data) {
      this.Form.patchValue({
        id: data.partner_id,
        partner_name: data.partner_name,
        partner_contact_no: data.partner_contact_no,
        partner_address: data.partner_address,
        partner_email_id: data.partner_email_id,
      });
    } else {
      this.Form.reset();
    }
  }


  addEditPartner() {
    // console.log("Form Data => ", this.Form.value, this.Form.value.length);
    if (this.Form.status === 'INVALID') return;
    let formData = this.Form.value;

    // Edit partner
    if (formData.id) {

      this.PartnerService.updatePartner(formData).subscribe(
        (success: any) => {
          // console.log(success);
          this.partner_modal.close();
          if (success.status == 1) {
            this.commonService.alertMsg("success", success.message, "Partner updated successfully!");
            this.getPartnerList();
          }
          else {
            this.partner_modal.close();
            this.commonService.alertMsg("error", success.message, "Failed updating Partner");
          }
        },
        fail => {
          console.log(fail);
          this.partner_modal.close();
          this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
        });
    } else if (!formData.id && formData.id == null) {
      delete formData['id'];
      // Add Partner
      this.PartnerService.addPartner(formData).subscribe(
        (success: any) => {
          // console.log(success);
          this.partner_modal.close();
          if (success.status == 1) {
            this.commonService.alertMsg("success", success.message, "Partner added successfully!");
            this.getPartnerList();
          } else {
            this.commonService.alertMsg("error", success.message, "Failed adding Partner");
          }
        },
        fail => {
          console.log(fail);
          this.partner_modal.close();
          this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
        });
    }

  }



  // Delete Partner
  showdeleteConfirmPopup(contentdelete: any, id: any) {
    // this.faqobj.id = id;
    this.id2Delete = id;
    this.partner_modal = this.modalService.open(contentdelete, {
      backdrop: 'static',
      keyboard: false
    });
  }

  showactivateConfirmPopup(contentdelete: any, id: any) {
    // this.faqobj.id = id;
    this.id2Delete = id;

    this.activate_modal = this.modalService.open(contentdelete, {
      backdrop: 'static',
      keyboard: false
    });
  }

  closedeletemodal() {
    this.id2Delete = null;
    this.partner_modal.close();
    this.modalService.dismissAll();
  }

  confirmDelete() {
    if (!this.id2Delete) return;

    if (this.id2Delete) {
      this.PartnerService.deletePartner(this.id2Delete, 0).subscribe(
        (success: any) => {

          this.partner_modal.close();
          if (success.status == 1) {
            this.commonService.alertMsg("success", "Partner deactivated successfully!");
            this.getPartnerList();
          } else {
            this.commonService.alertMsg("error", success.message, "Failed adding Partner");
          }
        },
        fail => {
          this.delete_modal.close();
          this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
        }
      )
    } else {
      this.delete_modal.close();
      this.commonService.alertMsg("error", "Something went wrong");
    }
  }// Delete partner ends

  confirmActivate() {
    if (!this.id2Delete) return;

    if (this.id2Delete) {
      this.PartnerService.deletePartner(this.id2Delete, 1).subscribe(
        (success: any) => {

          this.activate_modal.close();
          if (success.status == 1) {
            this.commonService.alertMsg("success", "Partner activated successfully!");
            this.getPartnerList();
          } else {
            this.commonService.alertMsg("error", success.message, "Failed adding Partner");
          }
        },
        fail => {
          this.delete_modal.close();
          this.commonService.alertMsg("error", fail.error.message, "Something went wrong");
        }
      )
    } else {
      this.delete_modal.close();
      this.commonService.alertMsg("error", "Something went wrong");
    }
  }// Delete partner ends


  viewRmPage(data: any) {
    // this.router.navigate([`/pendingorder/${data}`], { replaceUrl: true });
  }




} // Class Ends
