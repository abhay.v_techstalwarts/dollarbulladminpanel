// Ng Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing Module
import { PartnerRoutingModule } from './partner-routing.module';

// Other modules
import { SharedModule } from '../@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DataTablesModule } from 'angular-datatables';
import { EditorModule } from '@tinymce/tinymce-angular';

import { CustomersModule } from 'src/app/customers/customers.module';

// Components
import { PartnerListComponent } from './partner-list/partner-list.component';
import { PartnerRmListComponent } from './partner-rm-list/partner-rm-list.component'


@NgModule({
  declarations: [
    PartnerListComponent, 
    PartnerRmListComponent],
  imports: [
    CommonModule,
    PartnerRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    EditorModule,
    CustomersModule
  ]
})
export class PartnerModule { }
