import { JwtserviceService } from './../@core/service/jwt-token/jwtservice.service';
import { LoginServiceService } from './../@core/service/login/login-service.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../common.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  loginForm:any;
  rmForgotPasswordForm:any;
  constructor(private toastr: ToastrService, private commn : CommonService , private loginService : LoginServiceService , private jwtService : JwtserviceService, private modalService: NgbModal, config: NgbModalConfig ,private fb: FormBuilder,private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,]],
    });
    this.rmForgotPasswordForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
    });
  }
  

  clickLoginSubmit(){
    let data = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value,
    }
    

    this.loginService.Login(this.loginForm.value).subscribe(
      success => {  
      if(success.status == 1){
        this.jwtService.save_Mobile_number(success.result.mobile_no);
        this.jwtService.save_Email_ID(success.result.email);
        this.jwtService.saveToken(success.result.token);
        this.jwtService.save_Name(success.result.name);
        this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });

        this.router.navigate(['/stage-count'], { replaceUrl: true });
      }
      },
      error=>{
        // console.log("message");
        console.log(error);
        this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  submitForgot(){
    localStorage.setItem('email_id', this.rmForgotPasswordForm.get('email_id').value);
    let data = {
      email : this.rmForgotPasswordForm.get('email_id').value,
    }
    this.commn.forgotPasswordLink(data).subscribe((res) => {
      //@ts-ignore
      // console.log(res.message);
      //@ts-ignore
        this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
        this.rmForgotPasswordForm.reset();
    },
    (err) => {
      // this.showLoader = false;
      this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
    })
  }

  openForgotPasswordModal(forgotPasswordModal:any) {
    this.modalService.open(forgotPasswordModal);
  }

}
