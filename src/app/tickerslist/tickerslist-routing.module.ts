import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TickerslistComponent } from './tickerslist.component';

const routes: Routes = [{ path: '', component: TickerslistComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TickerRoutingModule { }
