import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TickerRoutingModule } from './tickerslist-routing.module';
import { TickerslistComponent } from './tickerslist.component';
import { SharedModule } from '../@shared/shared.module';


@NgModule({
  declarations: [
    TickerslistComponent
  ],
  imports: [
    CommonModule,
    TickerRoutingModule,
    SharedModule,
  ]
})
export class TickerListModule { }
