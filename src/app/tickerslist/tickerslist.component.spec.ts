import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerslistComponent } from './tickerslist.component';

describe('TickerslistComponent', () => {
  let component: TickerslistComponent;
  let fixture: ComponentFixture<TickerslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TickerslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
