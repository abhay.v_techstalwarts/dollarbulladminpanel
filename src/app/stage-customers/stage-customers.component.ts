import { Component, OnInit } from '@angular/core';
import { StageCustomersService } from '../@core/service/stage-customers/stage-customers.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonConstants } from 'src/app/@shared/constants/constants';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-stage-customers',
  templateUrl: './stage-customers.component.html'
})
export class StageCustomersComponent implements OnInit {
  stageCustomerList: any;
  search: any;
  dtOptions = CommonConstants.table;
  breadcrumb: any[] = [];
  dtTrigger: Subject<any> = new Subject<any>();
  type: any;
  title: any;
  stage: any;
  stage_data: any;
  stagecustomerobj = {
    "search": "",
    "type": ""
  }
  constructor(

    private stageCustomerService: StageCustomersService,
    private jwtService: JwtserviceService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  
  }

  ngOnInit(): void {

    this.search = "";
    this.type = this.route.snapshot.paramMap.get('id');



    this.stagecustomerobj = {
      "search": this.search,
      "type": this.type
    }
    this.getstageCustomerList(this.stagecustomerobj)
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';

  }

  ngAfterViewInit(): void {

    switch (this.type) {
      case '0':
        this.title = 'Signed Up Accounts';
        break;
      case '1':
        this.title = 'KYC Begin Accounts';
        break;
      case '2':
        this.title = 'Unverified Accounts';
        break;
      case '3':
        this.title = 'Margin Accounts';
        break;
      case '4':
        this.title = 'Verified Accounts';
        break;
      case '5':
        this.title = 'Funding Initiated Accounts';
        break;
      case '6':
        this.title = 'Funded Accounts';
        break;
    }
    this.breadcrumb = [{ name: "Dashboard", link: `/stage-count` },
    { name:  this.title }];
    this.dtOptions.buttons[0].title = this.title;
  }

  getstageCustomerList(search: any) {
    try {
      let offset = 0;
      let data = {
        offset: offset,
        limit: 200,
        search: search.search,
        type: search.type
      }
      this.stageCustomerService.stagewiseCustomerList(data).subscribe(
        (success: any) => {
          if (success.status == 1) {
            this.stageCustomerList = success.result.customer;
            this.dtTrigger.next();
            for (let i = 0; i < this.stageCustomerList.length; i++) {
              this.stageCustomerList[i].index = offset + (i + 1);
            }
          }
        },
        error => {
          console.log(error);
        }

      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  clear() {
    this.stagecustomerobj.search = "";

    this.getstageCustomerList(this.stagecustomerobj);
  }

}
