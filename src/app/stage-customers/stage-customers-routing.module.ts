import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StageCustomersComponent } from './stage-customers.component';

const routes: Routes = [{ path: '', component: StageCustomersComponent },
{ path: 'stage-count/stage-customers/:id', component: StageCustomersComponent },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StageCustomersRoutingModule { }
