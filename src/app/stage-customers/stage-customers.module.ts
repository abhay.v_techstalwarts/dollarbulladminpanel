import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, Params } from '@angular/router';
import { StageCustomersRoutingModule } from './stage-customers-routing.module';
import { StageCustomersComponent } from './stage-customers.component';
import { SharedModule } from '../@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DataTablesModule } from 'angular-datatables';




@NgModule({
  declarations: [
    StageCustomersComponent
  ],
  imports: [
    CommonModule,
    StageCustomersRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
  ]
})
export class StageCustomersModule { }
