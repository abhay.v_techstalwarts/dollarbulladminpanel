import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageCustomersComponent } from './stage-customers.component';

describe('StageCustomersComponent', () => {
  let component: StageCustomersComponent;
  let fixture: ComponentFixture<StageCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
