import { JwtService } from './../../jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
@Component({
  selector: 'app-investedstock',
  templateUrl: './investedstock.component.html',
  styleUrls: ['./investedstock.component.scss']
})
export class InvestedstockComponent implements OnInit {
  breadcrumb: any[] = []; 
  customer_Id:any;
  SelectAccountForm:any;
  stockList:any;
  tradeStation_ID:any;
  paramPartnerId:any;
  paramRmMasterId:any;
  paramCustomerId:any;
  accountList:any;
  tradestation_id:any;
  linkFromPartner: boolean = false;
  constructor(private fb: FormBuilder ,private jwtservice : JwtService, private activatedRoute: ActivatedRoute, private router: Router, private customerService: CustomerService) {
    this.breadcrumb[0] = {name: "Customer", link: `/customer`}; 
    this.breadcrumb[1] = {name: "Invested Stock"};
    this.paramPartnerId = this.activatedRoute.snapshot.params.partner_id;
    this.paramRmMasterId = this.activatedRoute.snapshot.params.rm_master_id;
    this.paramCustomerId = this.activatedRoute.snapshot.params.customer_id;
    this.linkFromPartner = (this.paramPartnerId && this.paramRmMasterId && this.paramCustomerId);
    if (this.linkFromPartner) this.PartnerToCustomer();
   }

  ngOnInit(): void {
    this.SelectAccountForm = this.fb.group({
      selectaccount: [''],
    });

    if( !this.linkFromPartner){
    this.customer_Id = this.activatedRoute.snapshot.paramMap.get('id');
   this.getlistOfInvestedStock();
    }
    else{
      this.customer_Id =  this.paramCustomerId;
      this.getlistOfInvestedStock();
    }
    this.customerAccount();
  }
  customerAccount() {
    let data = {
      customer_id: this.customer_Id,
    }
    this.customerService.getCustomerAccount(data).subscribe(
      success => {
        if (success.status == 1) {
          this.accountList = success.result;
          if(this.jwtservice.getTradeStationId()){
            this.selectAccount(this.jwtservice.getTradeStationId());
           this.SelectAccountForm.controls['selectaccount'].setValue(Number(this.jwtservice.getTradeStationId()));
           }

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  selectAccount(selectedAccount?: any) {
    // console.log(selectedAccount);
    let selectedItem = this.accountList.find((item: any) => item.customer_tradestation_account_id === Number(selectedAccount));
    this.tradestation_id = selectedItem.customer_tradestation_account_id;
    this.jwtservice.saveTradeStationId(this.tradestation_id);
    this.getlistOfInvestedStock();
  }

  getlistOfInvestedStock(){
    let data = {
      customer_id : this.customer_Id,
      customer_tradestation_account_id : this.jwtservice.getTradeStationId(),
    }
    this.customerService.getInvestedStocks(data).subscribe(
      success => {  
      if(success.status == 1){
        this.stockList = success.result.records;
        // console.log(this.stockList);


      }
      },
      error=>{
        // console.log("message");
        console.log(error);

      }

    )
  }

  PartnerToCustomer() {
    if (this.linkFromPartner) {
      this.breadcrumb = [];
      this.breadcrumb[0] = { name: "Partner", link: `/partner` };
      this.breadcrumb[1] = { name: "RM List", link: `/partner/rm/${this.paramPartnerId}` };
      this.breadcrumb[2] = { name: "Customer", link: `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}` };
      this.breadcrumb[3] = { name: "Invested Stock" };
    }
  }

}
