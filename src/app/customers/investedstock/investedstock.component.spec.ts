import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestedstockComponent } from './investedstock.component';

describe('InvestedstockComponent', () => {
  let component: InvestedstockComponent;
  let fixture: ComponentFixture<InvestedstockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestedstockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestedstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
