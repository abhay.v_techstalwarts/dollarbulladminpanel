import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersPendingOrderListComponent } from './customers-pending-order-list.component';

describe('CustomersPendingOrderListComponent', () => {
  let component: CustomersPendingOrderListComponent;
  let fixture: ComponentFixture<CustomersPendingOrderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomersPendingOrderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersPendingOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
