import { JwtService } from './../../jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { Subject } from 'rxjs';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { CommonConstants } from 'src/app/@shared/constants/constants';

@Component({
  selector: 'app-customers-pending-order-list',
  templateUrl: './customers-pending-order-list.component.html',
  styleUrls: ['./customers-pending-order-list.component.scss']
})
export class CustomersPendingOrderListComponent implements OnInit {
  customer_Id: any;
  pendingOrderList: any;
  dtOptions = CommonConstants.table;
  SelectAccountForm: any;
  dtTrigger: Subject<any> = new Subject<any>();
  accountList: any;
  tradestation_id: any;
  paramPartnerId: any = null;
  paramRmMasterId: any = null;
  paramCustomerId: any = null;
  breadcrumb: any[] = [];
  linkFromPartner: boolean = false;
  constructor(private fb: FormBuilder, private jwtservice: JwtService, private activatedRoute: ActivatedRoute, private router: Router, private customerService: CustomerService) {

    this.paramPartnerId = this.activatedRoute.snapshot.params.partner_id;
    this.paramRmMasterId = this.activatedRoute.snapshot.params.rm_master_id;
    this.paramCustomerId = this.activatedRoute.snapshot.params.customer_id;
    this.linkFromPartner = (this.paramPartnerId && this.paramRmMasterId && this.paramCustomerId);

    this.breadcrumb[0] = { name: "Customer", link: `/customer` };
    this.breadcrumb[1] = { name: "Pending Order" };

    if (this.linkFromPartner) this.PartnerToCustomer();

  }

  ngOnInit(): void {
    this.SelectAccountForm = this.fb.group({
      selectaccount: [''],
    });

    if (!this.linkFromPartner) {
      this.customer_Id = this.activatedRoute.snapshot.paramMap.get('id');
    }
    else {
      this.customer_Id = this.paramCustomerId;
    }

    this.customerAccount();




  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  customerAccount() {
    let data = {
      customer_id: this.customer_Id,
    }
    this.customerService.getCustomerAccount(data).subscribe(
      success => {
        if (success.status == 1) {
          this.accountList = success.result;
          if (this.jwtservice.getTradeStationId()) {
            this.selectAccount(this.jwtservice.getTradeStationId());
            this.SelectAccountForm.controls['selectaccount'].setValue(Number(this.jwtservice.getTradeStationId()));
          }

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }
  selectAccount(selectedAccount?: any) {
    // console.log(selectedAccount);
    let selectedItem = this.accountList.find((item: any) => item.customer_tradestation_account_id === Number(selectedAccount));
    this.tradestation_id = selectedItem.customer_tradestation_account_id;
    this.jwtservice.saveTradeStationId(this.tradestation_id);
    this.getlistOfCustomerPendingOrder();
  }


  getlistOfCustomerPendingOrder() {
    let data = {
      customer_id: this.customer_Id,
      customer_tradestation_account_id: this.jwtservice.getTradeStationId(),
    }
    this.customerService.getCustomerPendingOrderList(data).subscribe(
      success => {
        if (success.status == 1) {
          this.pendingOrderList = success.result.order;
          this.dtTrigger.next();

          // this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }


  PartnerToCustomer() {
    if (this.linkFromPartner) {
      this.breadcrumb = [];
      this.breadcrumb[0] = { name: "Partner", link: `/partner` };
      this.breadcrumb[1] = { name: "RM List", link: `/partner/rm/${this.paramPartnerId}` };
      this.breadcrumb[2] = { name: "Customer", link: `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}` };
      this.breadcrumb[3] = { name: "Pending Order" };
    }

  }



}
