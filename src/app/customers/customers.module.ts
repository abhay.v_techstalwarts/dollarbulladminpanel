import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersPortfolioComponent } from './customers-portfolio/customers-portfolio.component';
import { CustomersPendingOrderListComponent } from './customers-pending-order-list/customers-pending-order-list.component';
import { SharedModule } from '../@shared/shared.module';
import { ChartsModule } from 'ng2-charts';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvestedstockComponent } from './investedstock/investedstock.component';


@NgModule({
  declarations: [
    CustomersListComponent,
    CustomersPortfolioComponent,
    CustomersPendingOrderListComponent,
    InvestedstockComponent
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule,
    ChartsModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports:[
    CustomersListComponent,
    CustomersPortfolioComponent,
    CustomersPendingOrderListComponent
  ]
})
export class CustomersModule { }
