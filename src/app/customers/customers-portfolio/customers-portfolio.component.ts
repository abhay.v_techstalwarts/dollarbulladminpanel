import { JwtService } from './../../jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToastrService } from 'ngx-toastr';

import { SingleDataSet, Color, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';


@Component({
  selector: 'app-customers-portfolio',
  templateUrl: './customers-portfolio.component.html',
  styleUrls: ['./customers-portfolio.component.scss']
})
export class CustomersPortfolioComponent implements OnInit {
  customer_invested_value: any;
  customer_current_value: any;
  profit_loss_value: any;
  profit_loss_per: any;
  unrealised_pl = 0;
  realized_pl = 0;
  cash_available = 0;
  changerm_modalReference: any;
  delink_modalReference: any;
  deactivate_modalReference: any;
  customer_Id: any;
  data: any;
  dataLoaded: boolean = false;
  rmobj = {
    "id": ""
  };
  rmlist: any;
  rm_id: any;
  rmMasterID: any;
  paramPartnerId: any = null;
  paramRmMasterId: any = null;
  paramCustomerId: any = null;
  breadcrumb: any[] = [];
  SelectAccountForm: any;
  linkFromPartner: boolean = false;
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartOptions: ChartOptions = {
    responsive: true,

  };
  pieChartLabels = ['Stocks', 'ETFs'];
  pieChartValues = [30, 30];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // 

  lineChartData: ChartDataSets[] = [
    { data: [], label: '', fill: false, borderColor: '#3B64BE' },
  ];
  public lineChartLabels: any = [];
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: '#3B64BE',
      pointBackgroundColor: '#3B64BE',
      pointBorderColor: '#3B64BE',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public lineChartLegend = false;
  lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
  // 
  tradestation_id: any
  amount: any;
  accountList:any = [];
  selectedAccount: any;

  constructor(private fb: FormBuilder, private jwtservice: JwtService, private activatedRoute: ActivatedRoute, private router: Router, private customerService: CustomerService, private modalService: NgbModal,
    private editorService: EditorModule,
    private toastr: ToastrService) {
      // console.log(this.activatedRoute.snapshot.params);
    this.paramPartnerId = this.activatedRoute.snapshot.params.partner_id;
    this.paramRmMasterId = this.activatedRoute.snapshot.params.rm_master_id;
    this.paramCustomerId = this.activatedRoute.snapshot.params.customer_id;
    // console.log(this.paramCustomerId);

    this.linkFromPartner = (this.paramPartnerId && this.paramRmMasterId && this.paramCustomerId);



    if (this.linkFromPartner){
      this.PartnerToCustomer();
    } 
    else{
      this.breadcrumb[0] = { name: "Customer", link: `/customer` };
      this.breadcrumb[1] = { name: "Portfolio" };
    }

  }

  ngOnInit(): void {
    this.SelectAccountForm = this.fb.group({
      selectaccount: [''],
    });
    if (!this.linkFromPartner) {
      this.customer_Id = this.activatedRoute.snapshot.paramMap.get('id');
      this.customerAccount();
      this.rmList();

    }
    else {
      this.customer_Id = this.paramCustomerId;
      this.customerAccount();
      this.rmList();
    }
    // this.portfolioOverview();
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  rmList() {
    this.customerService.getRM().subscribe(
      success => {
        if (success.status == 1) {
          this.rmlist = success.result.rm;
          this.rmobj.id = "0";
        }
      })
  }

  selectRM(selectedRM?: any) {
    // console.log(selectedRM);

    // this.rmobj.id = selectedItem;
    // this.rmMasterID = selectedItem;

    // console.log(selectedRM, "rm master id")
  }

  confirmrm(content: any) {
    try {
      // console.log(this.paramCustomerId)
      let postData = {
        "customer_id": this.customer_Id,
        "rm_master_id": this.rmobj.id
      }
      this.customerService.updateRM(postData).subscribe(
        (success: any) => {
          // console.log(success);
          if (success.status == 1) {
            // this.faqobj.id = "";
            this.changerm_modalReference.close();
            this.toastr.success("success", "RM changed successfully");
            // this.getFAQ("");
          }
          else {
            this.changerm_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          this.changerm_modalReference.close();
          // console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }


    catch (err: any) {
      this.delink_modalReference.close();
      console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  portfolio() {
    let data = {
      customer_id: this.customer_Id,
      customer_tradestation_account_id: JSON.stringify(this.tradestation_id),
    }

    this.customerService.getPortfolio(data).subscribe(
      success => {
        if (success.status == 1) {
          this.pieChartLabels = Object.keys(success.result);
          // console.log(this.pieChartLabels);
          this.pieChartValues = Object.values(success.result);
          // this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  performanceChart() {
    let resul: any = {
      data: [],
      label: [],
    }
    let data = {
      customer_id: this.customer_Id,
      type: '1M',
      customer_tradestation_account_id: JSON.stringify(this.tradestation_id),
    }
    this.customerService.getPerformanceChart(data).subscribe(
      success => {


        if (success.status == 1) {
          success.result.forEach((element: any) => {
            this.lineChartData[0]['data']?.push(element.portfolio_value);
            this.lineChartLabels.push(element.record_date);

          });


        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  portfolioOverview() {
    let data = {
      customer_id: this.customer_Id,
      customer_tradestation_account_id: this.tradestation_id,
    }
    this.customerService.getPortfolioOverview(data).subscribe(
      success => {
        if (success.status == 1) {
          this.customer_invested_value = success.result.customer_invested_value;
          this.customer_current_value = success.result.customer_current_value;
          this.profit_loss_value = success.result.profit_loss_value;
          var pl_per = success.profit_loss_per;

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  customerAccount() {
    let data = {
      customer_id: this.customer_Id,
    }
    this.customerService.getCustomerAccount(data).subscribe(
      success => {
        if (success.status == 1) {
          this.accountList = success.result;
          if (this.jwtservice.getTradeStationId()) {
            this.selectAccount(this.jwtservice.getTradeStationId());
            this.SelectAccountForm.controls['selectaccount'].setValue(Number(this.jwtservice.getTradeStationId()));
          }

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  selectAccount(selectedAccount?: any) {
    // console.log(selectedAccount);
    let selectedItem = this.accountList.find((item: any) => item.customer_tradestation_account_id === Number(selectedAccount));
    this.tradestation_id = selectedItem.customer_tradestation_account_id;
    this.jwtservice.saveTradeStationId(this.tradestation_id);
    this.amount = selectedItem.account_balance;
    this.unrealised_pl = selectedItem.unrealized_pl;
    this.realized_pl = selectedItem.realized_pl;
    this.cash_available = selectedItem.account_balance;
    this.lineChartData[0]['data'] = [];
    this.lineChartLabels = [];
    this.performanceChart();
    this.portfolioOverview();
    this.portfolio();
    this.paramCustomerId = selectedItem.customer_id

    // console.log(selectedItem);
  }

  PartnerToCustomer() {
    if (this.linkFromPartner) {
      this.breadcrumb = [];
      this.breadcrumb[0] = { name: "Partner", link: `/partner` };
      this.breadcrumb[1] = { name: "RM List", link: `/partner/rm/${this.paramPartnerId}` };
      this.breadcrumb[2] = { name: "Customer", link: `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}` };
      this.breadcrumb[3] = { name: "Portfolio", link: `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}` };
    }
  }

  openChangeRMPopup(contentrm: any) {
    //this.faqobj.id = id;
    this.changerm_modalReference = this.modalService.open(contentrm, {
      backdrop: 'static',
      keyboard: false
    });
    this.changerm_modalReference.result.then((result2: any) => {
      // console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      // console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openDelinkPopup(contentdelink: any) {
    //this.faqobj.id = id;
    this.delink_modalReference = this.modalService.open(contentdelink, {
      backdrop: 'static',
      keyboard: false
    });
    this.delink_modalReference.result.then((result2: any) => {
      // console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      // console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closedelinkmodal(content: any) {
    this.modalService.dismissAll();

    // this.delink_modalReference.close();
  }

  confirmdelink(content: any) {
    try {
      console.log(this.paramCustomerId)
      let postData = {
        "customer_id": this.customer_Id
      }
      this.customerService.delinkCustomerTradeStation(postData).subscribe(
        (success: any) => {
          // console.log(success);
          if (success.status == 1) {
            // this.faqobj.id = "";
            this.delink_modalReference.close();
            this.toastr.success("Delinked successfully", "", { timeOut: 3000, disableTimeOut: false, });
            // this.getFAQ("");
          }
          else {
            this.delink_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          this.delink_modalReference.close();
          // console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }


    catch (err: any) {
      this.delink_modalReference.close();
      console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  openDeactivatePopup(contentdeactivate: any) {
    //this.faqobj.id = id;
    this.deactivate_modalReference = this.modalService.open(contentdeactivate, {
      backdrop: 'static',
      keyboard: false
    });
    this.deactivate_modalReference.result.then((result2: any) => {
      console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closedeactivatemodal(content: any) {
    this.deactivate_modalReference.close();
  }

  confirmdeactivate(content: any) {
    try {
      console.log(this.paramCustomerId)
      let postData = {
        "customer_id": this.customer_Id,
        is_active: 0
      }
      this.customerService.deactivateAccount(postData).subscribe(
        (success: any) => {
          console.log(success);
          if (success.status == 1) {
            // this.faqobj.id = "";
            this.deactivate_modalReference.close();
            this.toastr.success("Deactivated successfully", "", { timeOut: 3000, disableTimeOut: false, });
            // this.getFAQ("");
            this.router.navigate(['customer'], {replaceUrl:true});

          }
          else {
            this.deactivate_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          this.deactivate_modalReference.close();
          console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }


    catch (err: any) {
      this.delink_modalReference.close();
      console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }


}
