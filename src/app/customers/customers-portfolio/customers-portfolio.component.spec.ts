import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersPortfolioComponent } from './customers-portfolio.component';

describe('CustomersPortfolioComponent', () => {
  let component: CustomersPortfolioComponent;
  let fixture: ComponentFixture<CustomersPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomersPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
