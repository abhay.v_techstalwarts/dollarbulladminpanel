import { CustomersPortfolioComponent } from './customers-portfolio/customers-portfolio.component';
import { CustomersPendingOrderListComponent } from './customers-pending-order-list/customers-pending-order-list.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestedstockComponent } from './investedstock/investedstock.component';

const routes: Routes = [
  {path:'' , component : CustomersListComponent},
  {path:'pendingorder/:id' , component : CustomersPendingOrderListComponent},
  {path:'investedstock/:id' , component : InvestedstockComponent},
  {path:'portfolio/:id' , component : CustomersPortfolioComponent},
  {path:"rm/:partner_id/customer/:rm_master_id/pendingorder/:customer_id", component: CustomersPendingOrderListComponent},
  {path:"rm/:partner_id/customer/:rm_master_id/portfolio/:customer_id", component: CustomersPortfolioComponent},
  {path:"rm/:partner_id/customer/:rm_master_id/investedstock/:customer_id", component: InvestedstockComponent}



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
