import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subject } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { CustomerService } from 'src/app/@core/service/customer/customer.service';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { CommonConstants } from 'src/app/@shared/constants/constants';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {
  customersList: any;
  loader:boolean=true;
  dtOptions = CommonConstants.table;
  dtTrigger: Subject<any> = new Subject<any>();
  is_activestate:number=1;
  paramRmMasterId: any = null;
  paramPartnerId: any = null;
  breadcrumb: any[] = [];
  linkFromPartner: boolean = false;
  delete_modalReference: any;
  deactivate_modalReference: any;
  customer_id: any;
  pathPendingOrder: string = '/customer/pendingorder/';
  pathportfolio: string = '/customer/portfolio/';

  constructor(private customerService: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) {

    this.paramPartnerId = this.route.snapshot.params.partner_id;
    this.paramRmMasterId = this.route.snapshot.params.rm_master_id;
    this.linkFromPartner = (this.paramPartnerId && this.paramRmMasterId);

    if (this.linkFromPartner) this.PartnerToCustomer();

  }

  ngOnInit(): void {
    this.dtOptions.buttons[0].title = 'customer list';
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    //   processing: true,
    //   dom: 'Bfrtip',

    //   buttons: [
    //     { extend: 'excel', className: 'btn btn-outline-warning' },
    //   ]

    // };
    this.getlistOfCustomer();


  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtOptions.buttons[0].title = '';
  }
  getlistOfCustomer() {
    this.loader=true;
    this.customerService.getCustomerList(this.paramRmMasterId).subscribe(
      success => {
        if (success.status == 1) {
    this.loader=false;

          this.customersList = success.result.customer;
          this.dtTrigger.next();
          // console.log(this.customersList);

          // this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });

        }
      },
      error => {
        // console.log("message");
        console.log(error);
        // this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  ViewPendingOrderList(data: any) {
    this.router.navigate([this.pathPendingOrder + data]);
  }

  ViewPortfolio(data: any) {
    this.router.navigate([this.pathportfolio + data]);
  }

  // Partner to Customer
  PartnerToCustomer() {
    if (this.linkFromPartner) {
      this.breadcrumb = [];
      this.breadcrumb[0] = { name: "Partner", link: `/partner` };
      this.breadcrumb[1] = { name: "RM List", link: `/partner/rm/${this.paramPartnerId}` };
      this.breadcrumb[2] = { name: "Customer List" };

      this.pathPendingOrder = `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}/pendingorder/`;
      this.pathportfolio = `/partner/rm/${this.paramPartnerId}/customer/${this.paramRmMasterId}/portfolio/`;
    }

  }
  openDeactivatePopup(contentdeactivate: any, customer_id: any) {
    //this.faqobj.id = id;
    this.customer_id = customer_id;
    this.deactivate_modalReference = this.modalService.open(contentdeactivate, {
      backdrop: 'static',
      keyboard: false
    });
    this.deactivate_modalReference.result.then((result2: any) => {
      // console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      // console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closedeactivatemodal(content: any) {
    this.deactivate_modalReference.close();
  }

  confirmdeactivate(content: any ) {
    debugger;
    try {
      //    console.log(this.paramCustomerId)
     
      let postData = {
        "customer_id": this.customer_id,
        "is_active": 1,
      }
      this.customerService.deactivateAccount(postData).subscribe(
        (success: any) => {
          // console.log(success);
          if (success.status == 1) {
            // this.faqobj.id = "";
            this.deactivate_modalReference.close();
            this.toastr.success("Activated successfully", "", { timeOut: 3000, disableTimeOut: false, });
            this.getlistOfCustomer();
            // this.getFAQ("");
          }
          else {
            this.deactivate_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          this.deactivate_modalReference.close();
          // console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
      )
    }


    catch (err: any) {
      this.deactivate_modalReference.close();
      console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  // showdelinkConfirmPopup(id: any) {
  //   // this.stockObj.id = id;
  //   this.delete_modalReference = this.modalService.open(contentdelete, {
  //     backdrop: 'static',
  //     keyboard: false
  //   });
  //   this.delete_modalReference.result.then((result2: any) => {
  //     console.log("result", result2)
  //     // this.closeResult = `Closed with: ${result}`;
  //   }, (reason: any) => {
  //     console.log("reason", reason)
  //     //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //   });
  // }


}
