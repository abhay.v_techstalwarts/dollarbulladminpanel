import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FaqService } from '../@core/service/faq/faq.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToastrService } from 'ngx-toastr';
declare var tinymce: any;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
})
export class FaqComponent implements OnInit {
  faqList: any = [];
  faq_modalReference: any;
  delete_modalReference: any;
  faqForm: any;
  loader=true;
  search: any;
  faqobj = {
    "section": "",
    "question": "",
    "answer": "",
    "id": ""
  };

  constructor(
    private faqService: FaqService,
    private jwtService: JwtserviceService,
    private modalService: NgbModal,
    private editorService: EditorModule,
    private toastr: ToastrService
  ) { }

  onOptionsSelected(value: string) {
    this.getFAQ(value);
  }

  ngOnInit(): void {
    this.search = "";
    this.faqobj.section = "0";
    this.getFAQ("");

  }

  getFAQ(faq: any) {
    this.loader=true;
    try {
      let offset = 0;
      let data = {
        offset: offset,
        limit: 10,
        search: faq == '0' ? '' : faq
      }
      this.faqobj.section = "";
      this.faqobj.question = "";
      this.faqobj.answer = "";
      this.faqService.faqList(data).subscribe(
        (success: any) => {
          
          // console.log(success);
          if (success.status == 1) {
            this.loader=false;
            this.faqList = success.result.records;
            for (let i = 0; i < this.faqList.length; i++) {
              this.faqList[i].index = offset + (i + 1);
            }
            // console.log(data.search, ">>>>>>>>>> search");

            this.faqobj.section = data.search ? data.search : '0';
            // this.toastr.success(success.message, "Dollarbull", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          // console.log("message");
          console.log(error);
          //     this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

        }

      )


    }
    catch (err) {
      console.log(err);
    }
  }

  editModalFAQ(faq: any) {
    // console.log(faq)
    if (this.faqobj.section === "") {
      this.toastr.error("Please enter section", "", { timeOut: 3000, disableTimeOut: false, });

    }
    else if (this.faqobj.question === "") {
      this.toastr.error("Please enter question", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else if (this.faqobj.answer === "") {
      this.toastr.error("Please enter answer", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else {
      if (this.faqobj.id) {
        let data = {
          question: this.faqobj.question,
          section: this.faqobj.section,
          answer: this.faqobj.answer,
          id: this.faqobj.id
        }
        this.faqService.updateFAQ(data).subscribe((success: any) => {
          // console.log(success);
          this.faq_modalReference.close();
          if (success.status == 1) {
            this.toastr.success("FAQ updated successfully!", "", { timeOut: 3000, disableTimeOut: false });
            this.faqobj.id = "";
            this.getFAQ("");

          }
          else {
            this.faq_modalReference.close();
            this.toastr.error("Failed updating FAQ", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
          error => {
            this.faq_modalReference.close();
            console.log(error);
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          })
      }
      else {
        let data = {
          question: this.faqobj.question,
          section: this.faqobj.section,
          answer: this.faqobj.answer
        }
        this.faqService.addFAQ(data).subscribe((success: any) => {
          // console.log(success);
          this.faq_modalReference.close();
          if (success.status == 1) {
            // this.toastService.show('FAQ added successfully', { classname: 'bg-success text-light', delay: 10000 });
            this.toastr.success("FAQ added successfully!", "", { timeOut: 3000, disableTimeOut: false });
            this.getFAQ("");
          }
          else {
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
          error => {
            this.faq_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
            console.log(error);
          })
      }
    }
  }

  updateFAQ(faq: any, content_edit: any) {
    this.faqobj.section = faq.section;
    this.faqobj.question = faq.question;
    this.faqobj.answer = faq.answer;
    this.faqobj.id = faq.id;
    // this.faqobj.section = ;
    this.faq_modalReference = this.modalService.open(content_edit, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.faq_modalReference.result.then((result: any) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  showdeleteConfirmPopup(id: any, contentdelete: any) {
    this.faqobj.id = id;
    this.delete_modalReference = this.modalService.open(contentdelete, {
      backdrop: 'static',
      keyboard: false
    });
    this.delete_modalReference.result.then((result2: any) => {
      // console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      // console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closedeletemodal(content: any) {
    this.delete_modalReference.close();
  }

  confirmdelete(content: any) {
    try {
      if (this.faqobj.id) {
        this.faqService.deleteFAQ(this.faqobj.id).subscribe(
          (success: any) => {
            // console.log(success);
            if (success.status == 1) {
              this.faqobj.id = "";
              this.delete_modalReference.close();
              this.toastr.success("FAQ deleted successfully", "", { timeOut: 3000, disableTimeOut: false, });
              this.getFAQ("");
            }
            else {
              this.delete_modalReference.close();
              this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
            }
          },
          error => {
            this.delete_modalReference.close();
            // console.log("message");
            // console.log(error);
            this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
          }
        )
      }
      else {
        this.delete_modalReference.close();
        this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, })
      }
    }
    catch (err: any) {
      this.delete_modalReference.close();
      // console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  addFAQ() {
    if (this.faqobj.section === "") {
      this.toastr.error("Please enter section", "", { timeOut: 3000, disableTimeOut: false, });

    }
    else if (this.faqobj.question === "") {
      this.toastr.error("Please enter question", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else if (this.faqobj.answer === "") {
      this.toastr.error("Please enter answer", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else {
      let data = {
        question: this.faqobj.question,
        section: this.faqobj.section,
        answer: this.faqobj.answer
      }
      this.faqService.addFAQ(data).subscribe((success: any) => {
        // console.log(success);
        if (success.status == 1) {
          this.getFAQ("");
        }
        else {
          this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
        }
      },
        error => {
          this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          console.log(error);
        })
    }
  }

  openModel(content: any) {
    this.faqobj.section = "";
    this.faqobj.question = "";
    this.faqobj.answer = "";
    this.faqobj.section = "0";
    this.faq_modalReference = this.modalService.open(content, {
      backdrop: 'static',
      keyboard: false
    });
    this.faq_modalReference.result.then((result: any) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
}

