import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CustomNotificationService } from '../@core/service/custom-notification/custom-notification.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-custom-notification',
  templateUrl: './custom-notification.component.html'
})
export class CustomNotificationComponent implements OnInit {

  substage: any;
  title: any = "";
  body: any = "";
  substagelist: any;
  substageobj = {
    "id": 0,
    "title": ""
  };
  notifobj = {
    "substage": "",
    "title": "",
    "body": ""
  };

  constructor(
    private customNotificationService: CustomNotificationService,
    private jwtService: JwtserviceService,
    private modalService: NgbModal,
    private editorService: EditorModule,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getSubStage();
  }

  getSubStage() {
    this.customNotificationService.getSubStage().subscribe((success: any) => {
      // console.log(success)
      if (success.status == 1) {
        this.substagelist = success.result.records;
      }
    });
  }

  updateNotification(notification: any) {

    this.notifobj.title = notification.title;
    this.notifobj.body = notification.body;
    this.notifobj.substage = notification.substage;
    let data = {
      notification_title: this.notifobj.title,
      notification_text: this.notifobj.body,
      sub_stage_id: parseInt(this.notifobj.substage)
    }

    // this.faqobj.section = ;
    this.customNotificationService.pushNotification(data).subscribe((success: any) => {

      if (success.status == 1) {
        this.toastr.success("Notification sent successfully!", "", { timeOut: 3000, disableTimeOut: false });
        
        this.notifobj.title = '';
        this.notifobj.body = '';
        this.notifobj.substage = '';
      }
      else {

        this.toastr.error("Failed updating Notification", "", { timeOut: 3000, disableTimeOut: false, });
      }
    },
      error => {
        //  this.notif_modalReference.close();
        // console.log(error);
        this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
      })
  }

}
