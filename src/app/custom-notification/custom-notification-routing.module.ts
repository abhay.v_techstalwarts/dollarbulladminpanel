import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomNotificationComponent } from './custom-notification.component';

const routes: Routes = [{ path: '', component: CustomNotificationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomNotificationRoutingModule { }
