import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { CustomNotificationRoutingModule } from './custom-notification-routing.module';
import { CustomNotificationComponent } from './custom-notification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CustomNotificationComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    CustomNotificationRoutingModule,
    SharedModule
  ]
})
export class CustomNotificationModule { }
``