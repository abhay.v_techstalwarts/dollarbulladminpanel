import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoNotificationComponent } from './auto-notification.component';

describe('AutoNotificationComponent', () => {
  let component: AutoNotificationComponent;
  let fixture: ComponentFixture<AutoNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoNotificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
