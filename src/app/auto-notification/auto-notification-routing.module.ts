import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoNotificationComponent } from './auto-notification.component';

const routes: Routes = [{ path: '', component: AutoNotificationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutoNotificationRoutingModule { }
