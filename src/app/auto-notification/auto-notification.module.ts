import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { AutoNotificationRoutingModule } from './auto-notification-routing.module';
import { AutoNotificationComponent } from './auto-notification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AutoNotificationComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AutoNotificationRoutingModule,
    SharedModule
  ]
})
export class AutoNotificationModule { }
