import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AutoNotificationService } from '../@core/service/auto-notification/auto-notification.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auto-notification',
  templateUrl: './auto-notification.component.html'
})
export class AutoNotificationComponent implements OnInit {
  notifList: any = [];
  notif_modalReference: any;
  offset: any;
  variable: any;
  notifForm: any;
  loader=true;
  search: any;
  notifobj = {
    "title": "",
    "body": "",
    "variable": "",
    "hdnvariable": "",
    "notification_id": ""
  };
  constructor(
    private autoNotificationService: AutoNotificationService,
    private jwtService: JwtserviceService,
    private modalService: NgbModal,
    private editorService: EditorModule,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {


    this.getNotificationList();
  }

  getNotificationList() {
    this.loader=true;
    this.offset = 0;
    this.autoNotificationService.getNotificationList().subscribe(
      (success: any) => {
        if (success.status == 1) {
          this.loader=false;
          this.notifList = success.result.records;
          for (let i = 0; i < this.notifList.length; i++) {
            this.notifList[i].index = this.offset + (i + 1);
          }
        }
      },
      error => {
        console.log(error);
        //     this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

      }

    )
  }

  editModalNotif(notification: any) {


    console.log(notification)

    if (this.notifobj.title === "") {
      this.toastr.error("Please enter title", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else if (this.notifobj.body === "") {
      this.toastr.error("Please enter notification body", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else if (this.notifobj.hdnvariable != "" && this.notifobj.variable === "") {
      this.toastr.error("Please enter the variable", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else {
      if (this.notifobj.notification_id) {
        let data = {
          title: this.notifobj.title,
          body: this.notifobj.body,
          variable: this.notifobj.variable ? this.notifobj.variable : "",
          id: this.notifobj.notification_id
        }
        this.autoNotificationService.updateNotification(data).subscribe((success: any) => {
          this.notif_modalReference.close();
          if (success.status == 1) {
            this.toastr.success("Notification updated successfully!", "", { timeOut: 3000, disableTimeOut: false });
            this.notifobj.notification_id = "";
            this.getNotificationList();

          }
          else {
            this.notif_modalReference.close();
            this.toastr.error("Failed updating Notification", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
          error => {
            this.notif_modalReference.close();
            console.log(error);
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          })
      }

    }
  }

  updateNotification(notification: any, content_edit: any) {

    this.notifobj.title = notification.title;
    this.notifobj.body = notification.body;
    this.notifobj.variable = notification.variable;
    this.notifobj.hdnvariable = notification.variable;
    this.notifobj.notification_id = notification.notification_id;
    // this.faqobj.section = ;
    this.notif_modalReference = this.modalService.open(content_edit, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.notif_modalReference.result.then((result: any) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


}
