import { Component, OnInit } from '@angular/core';
import { StageCountService } from '../@core/service/stage-count/stage-count.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stage-count',
  templateUrl: './stage-count.component.html',
})
export class StageCountComponent implements OnInit {
  stageCount: any;
  constructor(
    private stageCountService: StageCountService,
    private jwtService: JwtserviceService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getStageCount();
  }


  getStageCount() {
    try {
      let offset = 0;
      let data = {
        offset: offset,
        limit: 10,
      }
      this.stageCountService.stageCount().subscribe(
        (success: any) => {
          if (success.status == 1) {
            this.stageCount = success.result;
          }
        },
        error => {
          console.log(error);
        }

      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  stagewiseCustomer(stage: any) {
    this.router.navigate([`stage-count/stage-customers/${stage}`], { replaceUrl: true });
  }
}
