import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StageCustomersComponent } from '../stage-customers/stage-customers.component';
import { StageCountComponent } from './stage-count.component';

const routes: Routes = [{ path: '', component: StageCountComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StageCountRoutingModule { }
