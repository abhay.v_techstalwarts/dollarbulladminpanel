import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { StageCountRoutingModule } from './stage-count-routing.module';
import { StageCountComponent } from './stage-count.component';


@NgModule({
  declarations: [
    StageCountComponent
  ],
  imports: [
    CommonModule,
    StageCountRoutingModule,
    SharedModule
  ]
})
export class StageCountModule { }
