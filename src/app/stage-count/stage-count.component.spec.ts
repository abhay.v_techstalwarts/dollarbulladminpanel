import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageCountComponent } from './stage-count.component';

describe('StageCountComponent', () => {
  let component: StageCountComponent;
  let fixture: ComponentFixture<StageCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
