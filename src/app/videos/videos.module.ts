import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { VideosRoutingModule } from './videos-routing.module';
import { VideosComponent } from './videos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    VideosComponent
  ],
  imports: [
    CommonModule,
    VideosRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class VideosModule { }
