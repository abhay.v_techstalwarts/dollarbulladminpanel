import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VideosService } from '../@core/service/videos/videos.service';
import { JwtserviceService } from '../@core/service/jwt-token/jwtservice.service';
import { NgbModal, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html'
})
export class VideosComponent implements OnInit {
  videoList: any = [];
  video_modalReference: any;
  delete_modalReference: any;
  hide_add:any=[];
  videoNamesList:any=[];
  addButtonVisibility:boolean=true;
  loader=true;
  videoForm: any;
  search: any;
  videoobj = {
    "type": "",
    "video": "",
    "id": ""
  };

  constructor(
    private videoService: VideosService,
    private jwtService: JwtserviceService,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private formsModule: FormsModule,
  ) { }

  onOptionsSelected(value: string) {
    this.getVideos(value);
  }

  ngOnInit(): void {
    this.search = "";
    this.videoobj.type = "0";
    this.getVideos("0");
  }

  getVideos(video: any) {
    try {
      let offset = 0;
      let data = {
        offset: offset,
        limit: 10,
        search: video
      }

      this.videoobj.type = "";
      this.videoobj.video = "";

      this.videoService.videoList(data).subscribe(
        (success: any) => {
          this.loader=true;
          this.videoNamesList=[];
          console.log(success);
          if (success.status == 1) {
            this.loader=false;
            this.videoList = success.result;
            this.hide_add = [... this.videoList];

            console.log(this.videoList);
            for (let i = 0; i < this.videoList.length; i++) {
              this.videoList[i].index = offset + (i + 1);
            
              // if( this.videoList[i].type.includes("KYC" , 'Partners' , 'About us', 'ujs'))
              // {
              //   console.log(true);
              // }
            }

            this.hide_add.forEach((element:any) => {
              this.videoNamesList.push(element.type);
             });
             console.log( this.videoNamesList);

             if(this.videoNamesList.includes("About us") && this.videoNamesList.includes("KYC") && this.videoNamesList.includes("Partners")) {
              console.log("yes");
              this.addButtonVisibility =false;
            }
            else{
              this.addButtonVisibility =true;

            }



            this.videoobj.type = data.search ? data.search : '0';
            // this.toastr.success(success.message, "Dollarbull", { timeOut: 3000, disableTimeOut: false, });
          }
        },
        error => {
          console.log("message");
          console.log(error);
          //     this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

        }

      )
    }
    catch (err: any) {
      console.log(err);
    }
  }

  editModalVideo(video: any) {
    console.log(video)
    if (this.videoobj.type === "") {
      this.toastr.error("Please enter section", "", { timeOut: 3000, disableTimeOut: false, });

    }
    else if (this.videoobj.video === "") {
      this.toastr.error("Please enter question", "", { timeOut: 3000, disableTimeOut: false, });
    }
    else {
      if (this.videoobj.id) {
        let data = {
          type: this.videoobj.type,
          url: this.videoobj.video,
          id: this.videoobj.id
        }
        this.videoService.updateVideo(data).subscribe((success: any) => {
          console.log(success);
          this.video_modalReference.close();
          if (success.status == 1) {
            this.toastr.success("Video updated successfully!", "", { timeOut: 3000, disableTimeOut: false });
            this.videoobj.id = "";
            this.getVideos("");

          }
          else {
            this.video_modalReference.close();
            this.toastr.error("Failed updating Video", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
          error => {
            this.video_modalReference.close();
            console.log(error);
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          })
      }
      else {
        let data = {
          type: this.videoobj.type,
          url: this.videoobj.video
        }
        this.videoService.addVideo(data).subscribe((success: any) => {
          console.log(success);
          this.video_modalReference.close();
          if (success.status == 1) {
            // this.toastService.show('Video added successfully', { classname: 'bg-success text-light', delay: 10000 });
            this.toastr.success("Video added successfully!", "", { timeOut: 3000, disableTimeOut: false });
            this.getVideos("");
          }
          else {
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
          }
        },
          error => {
            this.video_modalReference.close();
            this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
            console.log(error);
          })
      }
    }
  }

  updateVideo(video: any, content_edit: any) {
    this.videoobj.type = video.type;
    this.videoobj.video = video.url;
    this.videoobj.id = video.id;
    // this.Videoobj.section = ;
    this.video_modalReference = this.modalService.open(content_edit, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.video_modalReference.result.then((result: any) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  confirmdelete(content: any) {
    try {
      console.log(this.videoobj.id)
      if (this.videoobj.id) {
        this.videoService.deleteVideo(this.videoobj.id).subscribe(
          (success: any) => {
            console.log(success);
            if (success.status == 1) {
              this.videoobj.id = "";
              this.delete_modalReference.close();
              this.toastr.success("Video deleted successfully", "", { timeOut: 3000, disableTimeOut: false, });
              this.getVideos("");
            }
            else {
              this.delete_modalReference.close();
              this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
            }
          },
          error => {
            this.delete_modalReference.close();
            console.log("message");
            console.log(error);
            this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
          }
        )
      }
      else {
        this.delete_modalReference.close();
        this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, })
      }
    }
    catch (err: any) {
      this.delete_modalReference.close();
      console.log(err);
      this.toastr.error("Something went wrong", "", { timeOut: 3000, disableTimeOut: false, });
    }
  }

  closedeletemodal(content: any) {
    this.delete_modalReference.close();
  }

  showdeleteConfirmPopup(id: any, contentdelete: any) {
    this.videoobj.id = id;

    console.log(id, "delete")
    this.delete_modalReference = this.modalService.open(contentdelete, {
      backdrop: 'static',
      keyboard: false
    });
    this.delete_modalReference.result.then((result2: any) => {
      console.log("result", result2)
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      console.log("reason", reason)
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModel(content: any) {
    this.videoobj.id = "";
    this.videoobj.type = "0";
    this.videoobj.video = "";

    this.video_modalReference = this.modalService.open(content, {
      backdrop: 'static',
      keyboard: false
    });
    this.video_modalReference.result.then((result: any) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
